import tkinter as tk
import numpy as np
import turtle
import os
from turtle import Turtle, Screen
from PIL import Image, EpsImagePlugin, ImageOps

# CAMINHO DO EXE DO GHOSTSCRIPT
EpsImagePlugin.gs_windows_binary = r'C:\Program Files\gs\gs9.52\bin\gswin64c.exe'

# CONFIG DO TURTLE
screen = Screen()
t = Turtle("turtle")
t.pensize(35)
t.speed(-1)
t.shapesize(2,2,2)
t.fillcolor("green")
size = (28, 28)

def dragging(x, y):
    t.ondrag(None)
    t.setheading(t.towards(x, y))
    t.goto(x, y)
    t.ondrag(dragging)

def rigthclick(x, y):
    t.fillcolor("red")
    t.penup()

def midleclick(x, y):
    t.fillcolor("green")
    t.pendown()

def erase():
    t.clear()

# METODO QUE GERA O DESENHO PRA EPS, CONVERTE PRA PNG E IMPRIME O ARRAY DA IMAGEM
def imagearray():
    # QUANDO GERAR SETA O PINCEL PRETO PARA NAO INTERFERIR NA IMAGEM
    t.fillcolor("black")
    cleantrail()
    ts = turtle.getscreen()
    ts.getcanvas().postscript(file="number.eps")
    imgeps = Image.open("./number.eps").convert('L')
    imginvert = ImageOps.invert(imgeps)
    imgneg = imginvert.resize(size)
    imgarray = np.array(imgneg)
    print(imgarray.shape)
    imgarray1d = imgarray.flatten()
    print(imgarray1d)
    imgpng = Image.fromarray(imgarray)
    imgpng.save("number.png")
    t.fillcolor("red")
    t.penup()
    # APÓS O PROCESSAMENTO, DEIXA O PINCEL UP E VERMELHO

# METODO PARA LIMPAR OS ARQUIVOS ANTIGOS
def cleantrail():
    path = "C:\projetos\py-turtle"
    dir = os.listdir(path)
    for file in dir:
        if file == "number.png":
            print('clean number.png...')
            os.remove(file)
        elif file == "number.eps":
            print('clean number.eps...')
            os.remove(file)

def main():
    turtle.listen()
    t.ondrag(dragging)
    # CLICK DIREITO LEVANTA A CANETA
    turtle.onscreenclick(rigthclick, 3)
    # CLICK DO MEIO DESCE A CANETA
    turtle.onscreenclick(midleclick, 2)
    # ESPAÇO GERA A IMAGEM E O ARRAY
    turtle.onkey(imagearray, "space")
    # LETRA D LIMPA A TELA
    turtle.onkey(erase, "d")
    screen.mainloop()

main()